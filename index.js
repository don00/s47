const txt_first_name = document.querySelector('#txt-first-name')
const txt_last_name = document.querySelector('#txt-last-name')
const span_full_name = document.querySelector('#label-full-name')

txt_first_name.addEventListener('keyup', () => {
	if(!txt_last_name.value){
			span_full_name.innerHTML = txt_first_name.value
			return
	}

	span_full_name.innerHTML = txt_first_name.value + " " + txt_last_name.value

})


txt_last_name.addEventListener('keyup', () => {

	if(!txt_first_name.value){
		span_full_name.innerHTML = txt_last_name.value
		return
	}
	span_full_name.innerHTML = txt_first_name.value	+ " " + txt_last_name.value
})

